[TimeLoop]
DtInitial =  0.001 # [s]
MaxTimeStepSize = 3600 # [s] (12 hours)
TEnd = 86400 # [s] (6 d)
EpisodeLength = 3600

# for dune-subgrid
[Grid]
Positions0 = -0.5 0 1 1.5
Positions1 = 0 0.2 0.3 0.5
Cells0 = 10 250 10
Cells1 = 5 30 10
Grading0 = 1.0 1.0 1.0
Grading1 = -1.2 1.0 1.2
Baseline = 0.25 # [m]
Amplitude = 0.025 # [m]
Offset = 0.05 # [m]
Scaling = 0.2 #[m]

[Stokes.Grid]
Positions0 = -0.25 0.5
Positions1 = 0.25 0.5
Grading0 = 1.0
Grading1 = 1.5
Cells0 = 24
Cells1 = 16

[Darcy.Grid]
Positions0 = 0.0 0.25
Positions1 = 0.0 0.25
Cells0 = 8
Cells1 = 24
Grading0 = 1.0
Grading1 = -1.1

[Stokes.Problem]
Name = stokes_output_5cm
RefVelocity = 1.0 # [m/s]
RefPressure = 1e5 # [Pa]
refMoleFrac = 0.008 # [-]
RefTemperature = 283.15 # [K]
InitializationTime = 0.0

[Darcy.Problem]
Name = darcy_output_5cm
Pressure = 1.0e5
Saturation = 0.7 # initial Sw
Temperature = 283.15 # [K]
InitPhasePresence = 3 # bothPhases
InitializationTime = 0.0
PlexiglassThermalConductivity = 0.184 # [W/(m*K)] 0.116
PlexiglassThickness = 0.005 # [m]

[SpatialParams]
Porosity = 0.41
Permeability = 2.65e-10
AlphaBJ = 1.0
Swr = 0.005
Snr = 0.01
VgAlpha = 6.37e-4
VgN = 8.0

[Problem]
Name = test_stokes1p2cdarcy2p2chorizontal
EnableGravity = true

[Vtk]
AddVelocity = true
WriteFaceData = false

[Component]
SolidDensity = 2700
SolidThermalConductivity = 2.8
SolidHeatCapacity = 790

[RANS]
TurbulentSchmidtNumber = 0.7
TurbulentPrandtlNumber = 0.85

[Flux]
TvdApproach = Hou # 1: Uniform TVD, 2: Li's approach, 3: Hou's approach
DifferencingScheme = Vanleer # Vanleer , Vanalbada, Minmod, Superbee, Umist, Mclimiter, Wahyd

[Newton]
MaxSteps = 10
MaxRelativeShift = 1e-6

[Assembly]
NumericDifferenceMethod = 0
NumericDifference.BaseEpsilon = 1e-8
