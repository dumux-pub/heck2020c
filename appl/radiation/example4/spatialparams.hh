// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup OnePTests
 * \brief The spatial parameters class for the test problem using the 1p cc model
 */
#ifndef DUMUX_CONSERVATION_SPATIAL_PARAMS_HH
#define DUMUX_CONSERVATION_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>


namespace Dumux
{

/*!
 * \ingroup TwoPModel
 * \ingroup ImplicitTestProblems
 *
 * \brief The spatial parameters class for the test problem using the
 *        1p cc model
 */
template<class FVGridGeometry, class Scalar>
class TwoPTwoCSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar,
                             TwoPTwoCSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, TwoPTwoCSpatialParams<FVGridGeometry, Scalar>>;

    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

public:
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;
    using PermeabilityType = Scalar;


    TwoPTwoCSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
        : ParentType(fvGridGeometry)
    {
        permeability_ = getParam<Scalar>("SpatialParams.Permeability");
        porosity_ = getParam<Scalar>("SpatialParams.Porosity");
        alphaBJ_ = getParam<Scalar>("SpatialParams.AlphaBJ");

        // residual saturations
        params_.setSwr(getParam<Scalar>("SpatialParams.Swr"));
        params_.setSnr(getParam<Scalar>("SpatialParams.Snr"));
        // parameters for the vanGenuchten law
        params_.setVgAlpha(getParam<Scalar>("SpatialParams.VgAlpha"));
        params_.setVgn(getParam<Scalar>("SpatialParams.VgN"));

        upperLayerParams_.setSwr(getParam<Scalar>("SpatialParams.Swr"));
        upperLayerParams_.setSnr(getParam<Scalar>("SpatialParams.Snr"));
        // parameters for the vanGenuchten law
        upperLayerParams_.setVgAlpha(7.54332e-4);
        upperLayerParams_.setVgn(1.6);

        lowerParams_.setSwr(0.14);
        lowerParams_.setSnr(0.073);
        // parameters for the vanGenuchten law
        lowerParams_.setVgAlpha(6.6e-5);
        lowerParams_.setVgn(1.59);

    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *
     * \param globalPos The global position
     * \return the intrinsic permeability
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if (globalPos[1]<0.26)
            return 7.7e-14;
        else
            return permeability_;
    }

    /*! \brief Define the porosity in [-].
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    {
        if (globalPos[1]<0.26)
            return 0.398;
        else
            return porosity_;
    }

    /*! \brief Define the Beavers-Joseph coefficient in [-].
     *
     * \param globalPos The global position
     */
    Scalar beaversJosephCoeffAtPos(const GlobalPosition& globalPos) const
    { return alphaBJ_; }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law
     * which depends on the position
     *
     * \param globalPos The global position
     */
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
        if (globalPos[1]>0.47)
            return upperLayerParams_;
        else if
            (globalPos[1]<0.26)
            return lowerParams_;
        else
            return params_;
    }


   /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \return the wetting phase index
     * \param globalPos The global position
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

private:
    Scalar permeability_;
    Scalar porosity_;
    Scalar alphaBJ_;
    MaterialLawParams params_;
    MaterialLawParams lowerParams_;
    MaterialLawParams upperLayerParams_;
    static constexpr Scalar eps_ = 1.0e-7;

};

} // end namespace

#endif
