// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_DARCY_2P3C_RADIATION_SUBPROBLEM_HH
#define DUMUX_DARCY_2P3C_RADIATION_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/flux/maxwellstefanslaw.hh>

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/io/plotsolubility.hh>

#include "spatialparams.hh"
#include "radiation.hh"
#include <dumux/material/fluidsystems/h2on2co2ch4.hh>

namespace Dumux
{
template <class TypeTag>
class DarcySubProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct DarcyTwoPTwoCTypeTag { using InheritsFrom = std::tuple<TwoPNCNI, CCTpfaModel>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyTwoPTwoCTypeTag> { using type = Dumux::DarcySubProblem<TypeTag>; };


//The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyTwoPTwoCTypeTag>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using CO2Tables = Dumux::CO2Tables;
public:
  using type =  FluidSystems::H2ON2O2CO2CH4<Scalar, CO2Tables>;
};


//! Set the default formulation to pw-Sn: This can be over written in the problem.
template<class TypeTag>
struct Formulation<TypeTag, TTag::DarcyTwoPTwoCTypeTag>
{ static constexpr auto value = TwoPFormulation::p1s0; };

//// The gas component balance (air) is replaced by the total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::DarcyTwoPTwoCTypeTag> { static constexpr int value = 10; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyTwoPTwoCTypeTag> { using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::DarcyTwoPTwoCTypeTag> { static constexpr bool value = true; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyTwoPTwoCTypeTag> { using type = TwoPTwoCSpatialParams<TypeTag>; };

template<class TypeTag>
struct ThermalConductivityModel<TypeTag, TTag::DarcyTwoPTwoCTypeTag> { using type = ThermalConductivitySomerton<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct SetMoleFractionsForFirstPhase<TypeTag, TTag::DarcyTwoPTwoCTypeTag> { static constexpr bool value = false; };  //!< set mole fraction for 2nd, this means gas phase

// Set the grid type
template<class TypeTag>
struct MolecularDiffusionType<TypeTag, TTag::DarcyTwoPTwoCTypeTag> { using type =DIFFUSIONTYPE; };
}

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;


    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::H2OIdx,
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx
    };

    enum {energyEqIdx = Indices::energyEqIdx}; //water temp

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;

    using Radiation = typename Dumux::Radiation<Scalar, FVGridGeometry>;

public:
    DarcySubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Darcy"), eps_(1e-7), couplingManager_(couplingManager)
    {
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
        initialSw_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Saturation");
        temperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Temperature");
        initialPhasePresence_ = getParamFromGroup<int>(this->paramGroup(), "Problem.InitPhasePresence");

        initialCO2_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialCO2");
        initialCH4_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialCH4");

        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        time_ = 0.0;
        initializationTime_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitializationTime");

        diffCoeffAvgType_ = StokesDarcyCouplingOptions::stringToEnum(DiffusionCoefficientAveragingType{},
                                                                     getParamFromGroup<std::string>(this->paramGroup(), "Problem.InterfaceDiffusionCoefficientAvg"));

         plotSolubility();
    }

    void plotSolubility()
    {
       PlotSolubility<Scalar, FluidSystem>  plotSolubility;
       GnuplotInterface<Scalar> gnuplot;
       gnuplot.setOpenPlotWindow(true);
       plotSolubility.addSolubilityTemperatureDependent(gnuplot, 0, 4);
       gnuplot.plot("ch4-temp");
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }


    void setTime(Scalar time)
    { time_ = time; }

    const Scalar time() const
    {return time_; }

    /*!
     * \name Simulation steering
     */
    // \{

    template<class SolutionVector, class GridVariables>
    void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time)
    {
        if (time_>=initializationTime_)
        {
        // compute the mass in the entire domain
            Scalar massWater = 0.0;
            Scalar numScvf = 0.0;
            Scalar averageTemperature = 0.0;
            Scalar evaporation = 0.0;
            Scalar co2 = 0.0;
            Scalar ch4 = 0.0;
            Scalar energyFlux = 0.0;
            Scalar netRadiation = 0.0;
            std::vector<Scalar> diffusiveFluxes;
            std::vector<Scalar> totalFluxes;
            // bulk elements
            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bindElement(element);

                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);

                for (auto&& scv : scvs(fvGeometry))
                {
                    const auto& volVars = elemVolVars[scv];
                    for(int phaseIdx = 0; phaseIdx < FluidSystem::numPhases; ++phaseIdx)
                    {
                        massWater += volVars.massFraction(phaseIdx, FluidSystem::H2OIdx)*volVars.density(phaseIdx)
                        * scv.volume() * volVars.saturation(phaseIdx) * volVars.porosity() * volVars.extrusionFactor();
                    }
                }
                for (auto&& scvf : scvfs(fvGeometry))
                {
                    if (!couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
                        continue;

                    // NOTE: binding the coupling context is necessary
                    couplingManager_->bindCouplingContext(CouplingManager::darcyIdx, element);
                    const auto flux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

                    const auto fluxEnergy = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

                    const auto radiation = Radiation::radationEquilibrium(element, elemVolVars[scvf.insideScvIdx()], scvf,  couplingManager().couplingData().stokesTemperature(element, scvf), time);

                    evaporation += flux[0] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();

                    co2 += flux[3] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();

                    ch4 += flux[4] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();

                    energyFlux += fluxEnergy* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                    averageTemperature += elemVolVars[scvf.insideScvIdx()].temperature();
                    numScvf += 1;

                    netRadiation = radiation;

                    const auto diffusiveFlux = couplingManager().couplingData().diffusiveFluxDarcyBoundary(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

                    diffusiveFluxes.push_back(diffusiveFlux[4]* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()*FluidSystem::molarMass(4));

                    totalFluxes.push_back(flux[4]* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()*FluidSystem::molarMass(4));
                }
            }
            // convert to kg/s if using mole fractions
            evaporation = evaporation * FluidSystem::molarMass(FluidSystem::H2OIdx);
            co2 = co2 * FluidSystem::molarMass(3);
            ch4 = ch4 * FluidSystem::molarMass(4);
            averageTemperature = averageTemperature/numScvf;
            std::cout<<std::setprecision(12)<<"mass of water is: " << massWater << std::endl;
            std::cout<<"evaporation from pm "<<evaporation<<std::endl;
            std::cout<<"energyFlux from pm "<<energyFlux<<std::endl;
            std::cout<<"radiation "<<netRadiation<<std::endl;

            std::ofstream diffFlux;
            diffFlux.open(name()+"diffusiveFluxCH4.txt", std::ofstream::out | std::ofstream::app);
            diffFlux << time << "; ";
            std::copy(diffusiveFluxes.begin(), diffusiveFluxes.end(), std::ostream_iterator<Scalar>(diffFlux, ", "));
            diffFlux  << std::endl;
            diffFlux.close();

            std::ofstream totalFlux;
            totalFlux.open(name()+"totalFluxCH4.txt", std::ofstream::out | std::ofstream::app);
            totalFlux << time << "; ";
            std::copy(totalFluxes.begin(), totalFluxes.end(), std::ostream_iterator<Scalar>(totalFlux, ", "));
            totalFlux  << std::endl;
            totalFlux.close();

            //do a gnuplot
            x_.push_back(time); // in seconds
            y_.push_back(evaporation);

            gnuplot_.resetPlot();
            gnuplot_.setXRange(0,std::max(time, 86400.0));
            gnuplot_.setYRange(0, 1e-5);
            gnuplot_.setXlabel("time [s]");
            gnuplot_.setYlabel("kg/s");
            gnuplot_.addDataSetToPlot(x_, y_, name()+"evaporation");
//             gnuplot_.plot("evaporation");

            //do a gnuplot
            y2_.push_back(massWater);

            gnuplot2_.resetPlot();
            gnuplot2_.setXRange(0,std::max(time, 86400.0));
            gnuplot2_.setYRange(0, 20);
            gnuplot2_.setXlabel("time [s]");
            gnuplot2_.setYlabel("kg");
            gnuplot2_.addDataSetToPlot(x_, y2_, name()+"water mass");
//             gnuplot2_.plot("watermass");

            //do a gnuplot
            y3_.push_back(averageTemperature);

            gnuplot3_.resetPlot();
            gnuplot3_.setXRange(0,std::max(time, 86400.0));
            gnuplot3_.setYRange(280,300);
            gnuplot3_.setXlabel("time [s]");
            gnuplot3_.setYlabel("K");
            gnuplot3_.addDataSetToPlot(x_, y3_, name()+"temperatureAverage");
//             gnuplot3_.plot("temperature");

            //do a gnuplot
            y4_.push_back(netRadiation);
            gnuplot4_.resetPlot();
            gnuplot4_.setXRange(0,std::max(time, 86400.0));
            gnuplot4_.setYRange(0,600);
            gnuplot4_.setXlabel("time [s]");
            gnuplot4_.setYlabel("W/m^2");
            gnuplot4_.addDataSetToPlot(x_, y4_, name()+"net radiation");
//             gnuplot4_.plot("radiation");

            y5_.push_back(co2);
            gnuplot5_.resetPlot();
            gnuplot5_.setXRange(0,std::max(time, 86400.0));
            gnuplot5_.setYRange(0, 1e-5);
            gnuplot5_.setXlabel("time [s]");
            gnuplot5_.setYlabel("kg/s");
            gnuplot5_.addDataSetToPlot(x_, y5_, name()+"co2");

            y6_.push_back(ch4);
            gnuplot6_.resetPlot();
            gnuplot6_.setXRange(0,std::max(time, 86400.0));
            gnuplot6_.setYRange(0, 1e-5);
            gnuplot6_.setXlabel("time [s]");
            gnuplot6_.setYlabel("kg/s");
            gnuplot6_.addDataSetToPlot(x_, y6_, name()+"ch4");
        }
    }

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const //define output
    { return true; }

   // \}

     /*!
     * \name Boundary conditions
     */
    // \{
    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolumeFace &scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos = scvf.center();
        values.setAllNeumann();

        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
            values.setAllCouplingNeumann();
        else if (onLowerBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Dirichlet control volume.
     *
     * \param element The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary subcontrolvolumeface
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);
        values = initialAtPos(scvf.center());
//         values[switchIdx]= 0.8;
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann
     *        control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];
        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf) &&time_>=initializationTime_)
        {
            const auto massFlux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

            for(int i = 0; i< massFlux.size(); ++i)
                values[i] = massFlux[i];

            values[energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

            values[energyEqIdx] += -1*Radiation::radationEquilibrium(element, elemVolVars[scvf.insideScvIdx()], scvf,  couplingManager().couplingData().stokesTemperature(element, scvf),time());
        }
        else
        {
            Scalar temperatureInside = volVars.temperature();
            Scalar plexiglassThermalConductivity =
            getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PlexiglassThermalConductivity");
            Scalar plexiglassThickness =  getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PlexiglassThickness");
            values[energyEqIdx] = plexiglassThermalConductivity
                                  * (temperatureInside - temperature_)
                                  / plexiglassThickness;

        }

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * \param element The element for which the source term is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scv The subcontrolvolume
     *
     * For this method, the \a values variable stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    NumEqVector sourceAtPos(const GlobalPosition & globalPos) const
    {

        NumEqVector source(0.0);
//         if (globalPos[1] < 0.05)
//             source[conti0EqIdx+4] = 0.001;

        return source;



    }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        values.setState(initialPhasePresence_);
        FluidState fluidState;
        fluidState.setPressure(1, 1e5);
        fluidState.setTemperature(temperature_);
        fluidState.setMoleFraction(1, 1, 1-0.2-initialCO2_- initialCH4_);
        fluidState.setMoleFraction(1, 2, 0.2);
        fluidState.setMoleFraction(1, 3, initialCO2_);
        fluidState.setMoleFraction(1, 4, initialCH4_);
        Scalar density = FluidSystem::density(fluidState, 1);

        values[pressureIdx] = pressure_+ (9.81*density*(this->gridGeometry().bBoxMax()[1] - globalPos[1]));
        values[switchIdx] = initialSw_;
        values[conti0EqIdx+2] = 0.2; //o2
        values[conti0EqIdx+3] = initialCO2_; //co2
        values[conti0EqIdx+4] = initialCH4_; //ch4
        values[energyEqIdx] = temperature_; //20
        return values;
    }

    // \}

    /*!
     * \brief Set the coupling manager
     */
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    /*!
     * \brief Get the coupling manager
     */
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    Scalar pressure_;
    Scalar initialSw_;
    Scalar temperature_;
    Scalar initialCO2_;
    Scalar initialCH4_;
    int initialPhasePresence_;
    Scalar eps_;
    Scalar time_;
    Scalar initializationTime_;

    std::string problemName_;
    DiffusionCoefficientAveragingType diffCoeffAvgType_;
    std::shared_ptr<CouplingManager> couplingManager_;

    std::vector<double> x_;
    std::vector<double> y_;
    std::vector<double> y2_;
    std::vector<double> y3_;
    std::vector<double> y4_;
    std::vector<double> y5_;
    std::vector<double> y6_;
    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    Dumux::GnuplotInterface<double> gnuplot3_;
    Dumux::GnuplotInterface<double> gnuplot4_;
    Dumux::GnuplotInterface<double> gnuplot5_;
    Dumux::GnuplotInterface<double> gnuplot6_;
};
} //end namespace

#endif //DUMUX_DARCY2P2C_SUBPROBLEM_HH
