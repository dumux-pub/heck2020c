add_input_file_links()

dune_add_test(NAME test_column_maxwellstefan
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMPILE_DEFINITIONS DIFFUSIONTYPE=MaxwellStefansLaw<TypeTag>)

dune_add_test(NAME test_column_fickslaw
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMPILE_DEFINITIONS DIFFUSIONTYPE=FicksLaw<TypeTag>)
