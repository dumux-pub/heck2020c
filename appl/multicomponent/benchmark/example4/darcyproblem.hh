// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Darcy-Subproblem with constant injection of gas at a soil tank bottom (cell-centered finite volume method).
 */
#ifndef DUMUX_DARCY_SUBPROBLEM_TWOPNC_GASINJECTION_HH
#define DUMUX_DARCY_SUBPROBLEM_TWOPNC_GASINJECTION_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>

#include <dumux/flux/maxwellstefanslaw.hh>

#include <dumux/io/gnuplotinterface.hh>

#include <dumux/porousmediumflow/1pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "spatialparams.hh"

#include <dumux/material/fluidsystems/h2on2o2co2ch4_fixedcoefficients.hh>
#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

namespace Dumux
{
template <class TypeTag>
class DarcySubProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct DarcyOnePThreeC { using InheritsFrom = std::tuple<OnePNC, CCTpfaModel>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyOnePThreeC> { using type = Dumux::DarcySubProblem<TypeTag>; };

//The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyOnePThreeC>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using CO2Tables = Dumux::CO2Tables;
public:
  using H2ON2O2CO2CH4 = FluidSystems::H2ON2O2CO2CH4<Scalar, CO2Tables>;
  static constexpr auto phaseIdx = H2ON2O2CO2CH4::gasPhaseIdx; // simulate the gas phase
  using type = FluidSystems::OnePAdapter<H2ON2O2CO2CH4, phaseIdx>;
};

// Use moles
template<class TypeTag>
struct UseMoles<TypeTag, TTag::DarcyOnePThreeC> { static constexpr bool value = true; };

// Do not replace one equation with a total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::DarcyOnePThreeC> { static constexpr int value = 10; };

//! Use a model with constant tortuosity for the effective diffusivity
template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::DarcyOnePThreeC>
{ using type = DiffusivityConstantTortuosity<GetPropType<TypeTag, Properties::Scalar>>; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyOnePThreeC> { using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

// Set the spatial paramaters type
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyOnePThreeC>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = OnePSpatialParams<FVGridGeometry, Scalar>;
};

// Set the diffusion type
template<class TypeTag>
struct MolecularDiffusionType<TypeTag, TTag::DarcyOnePThreeC> { using type = DIFFUSIONTYPE; };
}

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;
    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx,

        //componentIndices
        H2OIdx = 1, //switched because of OnePAdapter
        N2Idx = 0,
        O2Idx = 2,
        CO2Idx = 3,
        CH4Idx = 4
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
public:
    DarcySubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Darcy"), eps_(1e-7), couplingManager_(couplingManager)
    {
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        initialTemperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitialTemperature");

        diffCoeffAvgType_ = StokesDarcyCouplingOptions::stringToEnum(DiffusionCoefficientAveragingType{},
                                                            getParamFromGroup<std::string>(this->paramGroup(), "Problem.InterfaceDiffusionCoefficientAvg"));
        injectedVolumeFlux_ =  getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InjectedVolumeFlux");
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    void setTime( Scalar time )
    {
        time_ = time;
    }


    template<class SolutionVector, class GridVariables>
    void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time)
    {
       // compute the methane flux across the boundary
        Scalar ch4 = 0.0;
        Scalar diffusiveFluxCH4 = 0.0;
        Scalar o2 = 0.0;

        // bulk elements
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, curSol);

            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (!couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
                    continue;

                // NOTE: binding the coupling context is necessary
                couplingManager_->bindCouplingContext(CouplingManager::darcyIdx, element);
                const auto flux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

                ch4 += flux[4] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                o2 +=  flux[2] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                diffusiveFluxCH4 += (couplingManager().couplingData().diffusiveFluxDarcyBoundary(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_)[4])* scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
            }
        }
        // convert to kg/s if using mole fractions
        ch4 = ch4 * FluidSystem::molarMass(4);
        diffusiveFluxCH4 = diffusiveFluxCH4 * FluidSystem::molarMass(4);
        std::cout<<"ch4 from pm "<<ch4<<std::endl;
        o2 = o2 * FluidSystem::molarMass(2);
        std::cout<<"o2 from pm "<<o2<<std::endl;


         //do a gnuplot
        x_.push_back(time); // in seconds
        y1_.push_back(diffusiveFluxCH4);
        y2_.push_back(ch4);

        //do a gnuplot
        y3_.push_back(o2);

        gnuplot1_.resetPlot();
        gnuplot1_.setXRange(0,std::max(time, 300.0));
        gnuplot1_.setYRange(0, 5e-9);
        gnuplot1_.setXlabel("time [s]");
        gnuplot1_.setYlabel("kg/s");
        gnuplot1_.addDataSetToPlot(x_, y1_, "diffFlux_50ml_109_grav");

        gnuplot2_.resetPlot();
        gnuplot2_.setXRange(0,std::max(time, 300.0));
        gnuplot2_.setYRange(0, 5e-9);
        gnuplot2_.setXlabel("time [s]");
        gnuplot2_.setYlabel("kg/s");
        gnuplot2_.addDataSetToPlot(x_, y2_, "ch4_50ml_109_grav");

        gnuplot3_.resetPlot();
        gnuplot3_.setXRange(0,std::max(time, 300.0));
        gnuplot3_.setYRange(0, 5e-9);
        gnuplot3_.setXlabel("time [s]");
        gnuplot3_.setYlabel("kg/s");
        gnuplot3_.addDataSetToPlot(x_, y3_, "o2_50ml_109_grav");
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const // define output
    { return true; }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return initialTemperature_; } // 20°C
    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
      * \brief Specifies which kind of boundary condition should be
      *        used for which equation on a given boundary control volume.
      *
      * \param element The element
      * \param scvf The boundary sub control volume face
      */
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        auto globalPos = scvf.ipGlobal();
        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
            values.setAllCouplingNeumann();

        //bottom for injection
        if (globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_)
               values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        auto globalPos = scvf.ipGlobal();
        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
        {
            const auto massFlux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf, diffCoeffAvgType_);

            for(int i = 0; i< massFlux.size(); ++i)
                values[i] = massFlux[i];
        }

        if (globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_)
        {
            FluidState fluidState;
            fluidState.setPressure(0, 1e5);
            fluidState.setTemperature(temperature());
            Scalar moleFractionCH4 = 0.05;
            Scalar moleFractionO2 = 0.2199;
            Scalar moleFractionCO2 = 0.0;
            Scalar moleFractionH2O = 0.0;
            Scalar moleFractionN2 = 1-moleFractionO2-moleFractionCH4;

            fluidState.setMoleFraction(0, N2Idx, moleFractionN2);
            fluidState.setMoleFraction(0, H2OIdx, moleFractionH2O);
            fluidState.setMoleFraction(0, O2Idx, moleFractionO2);
            fluidState.setMoleFraction(0, CO2Idx, moleFractionCO2);
            fluidState.setMoleFraction(0, CH4Idx, moleFractionCH4);

            Scalar density = FluidSystem::molarDensity(fluidState, 0);

            Scalar area = (0.2/2)*(0.2/2)*M_PI;

            //injectedVolumeFlux in ml/min *1e-6 to convert to m^3/min
            Scalar molarFlux = injectedVolumeFlux_*1e-6/60*density/area;

            values[conti0EqIdx + N2Idx] = -molarFlux*moleFractionN2;

            values[conti0EqIdx + O2Idx] = -molarFlux*moleFractionO2;

            values[conti0EqIdx + CH4Idx] = -molarFlux*moleFractionCH4;

        }
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param values Stores the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        values[Indices::pressureIdx] = pressure_;
        values[Indices::conti0EqIdx+2] =  0.148;
        values[Indices::conti0EqIdx+4] =  0.05;
        return values;
    }

       /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);
        return values;

    }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param element The element
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

        values[Indices::pressureIdx] = pressure_;
        values[Indices::conti0EqIdx+2] = 0.21;
        return values;
    }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    bool injBool_;
    Scalar eps_;
    Scalar pressure_;
    Scalar initialMoleFraction_;
    Scalar initialMoleFractionGas_;
    Scalar initialTemperature_;
    Scalar injectedVolumeFlux_;
    Scalar epsInj_;
    Scalar stokesTop_;
    Scalar time_ = 0.0;

    std::vector<double> x_;
    std::vector<double> y3_;
    std::vector<double> y2_;
    std::vector<double> y1_;
    Dumux::GnuplotInterface<double> gnuplot3_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    Dumux::GnuplotInterface<double> gnuplot1_;

    DiffusionCoefficientAveragingType diffCoeffAvgType_;

    std::string problemName_;
    std::shared_ptr<CouplingManager> couplingManager_;
};
} //end namespace

#endif //DUMUX_DARCY_SUBPROBLEM_HH
