[TimeLoop]
DtInitial =  0.01 # [s]
MaxTimeStepSize = 100 # [s] (12 hours)
EpisodeLength = 100
TEnd = 3600 # [s] (6 hours)

# for dune-subgrid
[Grid]
Positions0 = -0.5 -0.1 0.1 0.6 1.5
Positions1 = 0 0.17 0.2 0.27 0.5
Cells0 = 15 60 150 25
Cells1 = 10 25 30 10
Grading0 = 1.0 1.0 1.0 1.0
Grading1 = -1.2 -1.05 1.05 1.2

StartDarcy = 0.0 0.2
StartFormOne = 0.0
EndFormOne = 0.1
EndDarcy = 0.6 0.2
Amplitude = 0.025
CurveBuffer = 0.008

[Stokes.Grid]
Positions0 = -0.25 0.0 0.25 0.5
Positions1 = 0.25 0.5
Grading0 = -1.05 1.0 1.05
Grading1 = 1.1
Cells0 = 20 40 20
Cells1 = 30

[Darcy.Grid]
Positions0 = 0.0 0.25
Positions1 = 0.0 0.25
Cells0 = 40
Cells1 = 30
Grading0 = 1.0
Grading1 = -1.1

[Stokes.Problem]
Name = stokes_silt_sw03_ch4_005_rectangle_2cm
RefVelocity = 1 # [m/s]
RefPressure = 1e5 # [Pa]
refMoleFrac = 0.008 # [-]
RefTemperature = 283.15 # [K]
InitializationTime = 0.001

[Darcy.Problem]
Name = darcy_silt_sw03_ch4_005_rectangle_2cm
Pressure = 1.0e5
Saturation = 0.3 # initial Sw
Temperature = 283.15 # [K]
InitPhasePresence = 3 # bothPhases
InitializationTime = 0.001
PlexiglassThermalConductivity = 0.184 # [W/(m*K)] 0.116
PlexiglassThickness = 0.005 # [m]
InitialCO2 = 0.05
InitialCH4 = 0.05

[SpatialParams]
Porosity = 0.35
Permeability = 1.08e-12
AlphaBJ = 1.0
Swr = 0.057
Snr = 0.029
VgAlpha = 4.28e-5
VgN = 1.32

[Problem]
Name = test_stokes1p2cdarcy2p2chorizontal
EnableGravity = true
InterfaceDiffusionCoefficientAvg = FreeFlowOnly # only harmonic and ffonly are possible

[Vtk]
AddVelocity = true
WriteFaceData = false
OutputName = test_silt


[Component]
SolidDensity = 2700
SolidThermalConductivity = 2.8
SolidHeatCapacity = 790

[RANS]
TurbulentSchmidtNumber = 0.7
TurbulentPrandtlNumber = 0.85

[Flux]
TvdApproach = Hou # 1: Uniform TVD, 2: Li's approach, 3: Hou's approach
DifferencingScheme = Vanleer # Vanleer , Vanalbada, Minmod, Superbee, Umist, Mclimiter, Wahyd

[Newton]
MaxSteps = 10
MaxRelativeShift = 1e-6

[Assembly]
NumericDifferenceMethod = 0
NumericDifference.BaseEpsilon = 1e-8

