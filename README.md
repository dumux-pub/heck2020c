SUMMARY
=======
This is the DuMuX module containing the code for producing the results which are part of the dissertation
submitted for:

Dissertation 2020
K. Heck<br>
Modelling and analysis of multicomponent transport at the interface between free- and porous-medium flow - influenced by radiation and roughness


Installation
============

The easiest way to install this module is to create a new directory and download `installheck2020c.sh` from this repository.

```
mkdir heck2020c && cd heck2020c
chmod +x installheck2020c.sh
./installHeck2020c.sh
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run. For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installHeck2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Heck2020a/raw/master/installHeck2020a.sh).


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir heck2020c
cd heck2020c
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/heck2020c/-/raw/master/docker_heck2020c.sh
```

Open the Docker Container
```bash
bash docker_heck2020c.sh open
```

Applications
============

You can find all examples in the appl folder. you may build all executables by running

```bash
cd heck2020c/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths

- appl/radiation_multicomponent/radiation_obstacle_multicomponent
- appl/radiation_multicomponent/radiation_flat_multicomponent_laminar
- appl/radiation_multicomponent/radiation_flat_multicomponent
- appl/radiation/example1
- appl/radiation/example2
- appl/radiation/example2_turbulent
- appl/radiation/example3
- appl/radiation/example4
- appl/radiation/example4_diffyear
- appl/multicomponent/singlephase/windcanal_laminar
- appl/multicomponent/singlephase/windcanal_complexfluidsystem
- appl/multicomponent/singlephase/windcanal
- appl/multicomponent/multiphase/obstacle_multicomponent
- appl/multicomponent/multiphase/flat_multicomponent_laminar
- appl/multicomponent/multiphase/flat_multicomponent

They can be executed with an input file e.g., by running

```bash
cd appl/radiation_multicomponent/radiation_obstacle_multicomponent
./test_stokes1p2cdarcy2p3c_radiation_wavy_maxwellstefan params_silt.input
```
