/*****************************************************************************
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Binary coefficients for CH4 and carbondioxide.
 */
#ifndef DUMUX_BINARY_COEFF_CH4_CO2_HH
#define DUMUX_BINARY_COEFF_CH4_CO2_HH

#include <dumux/material/binarycoefficients/henryiapws.hh>
#include <dumux/material/binarycoefficients/fullermethod.hh>

#include <dumux/material/components/co2.hh>
#include <dumux/material/components/ch4.hh>
#include <dumux/material/components/n2.hh>
#include <dumux/material/components/o2.hh>

namespace Dumux
{
namespace BinaryCoeff
{

/*!
 * \brief Binary coefficients for CH4 and carbondioxide.
 */
class CH4_CO2
{
public:


    /*!
     * \brief Binary diffusion coefficent [m^2/s] for molecular CH4 and carbondioxide.
     *
     * \copybody fullerMethod()
     */
    //template <class Scalar, class CO2Tables>
    template <class Scalar>
    static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)

    {
        typedef Components::CH4<Scalar> CH4;
        //typedef Dumux::CO2<Scalar, CO2Tables> CO2;
        //typedef Dumux::N2<Scalar> N2;
        //typedef Dumux::O2<Scalar> O2;

        // atomic diffusion volumes
        const Scalar SigmaNu[2] = {  25.14  /* CH4 */, 26.9 /* CO2 */};
        // molar masses [g/mol]
        //const Scalar M[2] = { N2::molarMass()*1e3, O2::molarMass()*1e3, CO2::molarMass()*1e3 };
        const Scalar M[2] = {CH4::molarMass()*1e3, 0.044*1e3 };
        return fullerMethod(M, SigmaNu, temperature, pressure);
    };

    /*!
     * \brief Diffusion coefficent [m^2/s] for molecular carbondioxide in liquid CH4.
     */
    template <class Scalar>
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        return 1e-26; //this is a dummy value to make maxwell stefan run, set it to a low value assuming there is not much influence of these two in water
        DUNE_THROW(Dune::NotImplemented, "diffusion coefficient for liquid carbondioxide and CH4");
    };
};

}
} // end namespace

#endif
