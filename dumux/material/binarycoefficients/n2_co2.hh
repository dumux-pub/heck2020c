/*****************************************************************************
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Binary coefficients for air and carbondioxide.
 */
#ifndef DUMUX_BINARY_COEFF_N2_CO2_HH
#define DUMUX_BINARY_COEFF_N2_CO2_HH

#include <dumux/material/binarycoefficients/henryiapws.hh>
#include <dumux/material/binarycoefficients/fullermethod.hh>

#include <dumux/material/components/co2.hh>
#include <dumux/material/components/n2.hh>
#include <dumux/material/components/o2.hh>

namespace Dumux
{
namespace BinaryCoeff
{

/*!
 * \brief Binary coefficients for air and carbondioxide.
 */
class N2_CO2
{
public:
    /*!
     * \brief Henry coefficient \f$[N/m^2]\f$  for molecular carbondioxide in liquid air.
     */
    template <class Scalar>
    static Scalar henry(Scalar temperature)
    {
        DUNE_THROW(Dune::NotImplemented, "henry coefficient for carbondioxide in liquid air");
    };

    /*!
     * \brief Binary diffusion coefficent [m^2/s] for molecular air and carbondioxide.
     *
     * \copybody fullerMethod()
     */
    //template <class Scalar, class CO2Tables>
    template <class Scalar>
    static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)

    {
        // atomic diffusion volumes
        const Scalar SigmaNu[2] = { 18.1 /* N2 */, 26.9 /* CO2 */};
        // molar masses [g/mol]
        //const Scalar M[2] = { N2::molarMass()*1e3, O2::molarMass()*1e3, CO2::molarMass()*1e3 };
        const Scalar M[2] = { 0.028*1e3, 0.044*1e3 };
        return fullerMethod(M, SigmaNu, temperature, pressure);
    };

    /*!
     * \brief Diffusion coefficent [m^2/s] for molecular carbondioxide in liquid air.
     */
    template <class Scalar>
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        return 1e-26; //this is a dummy value to make maxwell stefan run, set it to a low value assuming there is not much influence of these two in water
        DUNE_THROW(Dune::NotImplemented, "diffusion coefficient for liquid carbondioxide and n2");
    };
};

}
} // end namespace

#endif
