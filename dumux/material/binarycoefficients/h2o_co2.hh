/*****************************************************************************
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Binary coefficients for CO2 and brine.
 */
#ifndef DUMUX_BINARY_COEFF_H2O_CO2_HH
#define DUMUX_BINARY_COEFF_H2O_CO2_HH

#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/co2.hh>
#include <dumux/material/idealgas.hh>

namespace Dumux {
namespace BinaryCoeff {
/*!
 * \brief Binary coefficients for H2O and CO2.
 */

class H2O_CO2 {
public:
    /*!
     * \brief Henry coefficient \f$\mathrm{[Pa]}\f$  for molecular nitrogen in liquid water.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     */
    template <class Scalar>
    static Scalar henry(Scalar temperature)
    {
        const Scalar E = 1672.9376;
        const Scalar F =  28.1751;
        const Scalar G = -112.4619;
        const Scalar H = 85.3807;

        return henryIAPWS(E, F, G, H, temperature);
    }

    /*!
     * \brief Binary diffusion coefficent [m^2/s] of water in the CO2 phase.
     */
    template <class Scalar>
    static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
    {
       using H2O = Components::H2O<Scalar>;

        // atomic diffusion volumes
        const Scalar SigmaNu[2] = { 13.1 /* H2O */,  26.9 /* CO2 */ };
        // molar masses [g/mol]
        const Scalar M[2] = { H2O::molarMass()*1e3, 44e-3*1e3 };

        return fullerMethod(M, SigmaNu, temperature, pressure);
    } ;

       /*!
     * \brief Diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular methane in liquid water.
     * \param temperature the temperature \f$\mathrm{[K]}\f$
     * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
     *
     * The empirical equations for estimating the diffusion coefficient in
     * infinite solution which are presented in Reid, 1987 \cite reid1987 all show a
     * linear dependency on temperature. We thus simply scale the
     * experimentally obtained diffusion with the temperature
    */
    template <class Scalar>
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        const Scalar Texp = 273.15 + 25; // [K] see reid et al p. 665
        const Scalar Dexp = 2.0e-9; // [m^2/s]
        return Dexp * temperature/Texp;
    }

};
}
} // end namespace

#endif
