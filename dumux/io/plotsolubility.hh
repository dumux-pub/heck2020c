// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup InputOutput
 * \brief Interface for plotting the two-phase fluid-matrix-interaction laws
 */
#ifndef DUMUX_PLOT_SOLUBILITY_LAW_HH
#define DUMUX_PLOT_SOLUBILITY_LAW_HH

#include <cmath>
#include <vector>
#include <string>
#include <dumux/material/fluidstates/compositional.hh>

#include <dumux/material/binarycoefficients/h2o_n2.hh>
#include <dumux/material/binarycoefficients/h2o_o2.hh>
#include <dumux/material/binarycoefficients/n2_o2.hh>
#include <dumux/material/binarycoefficients/h2o_co2.hh>
#include <dumux/material/binarycoefficients/h2o_ch4.hh>

namespace Dumux {

// forward declaration
template<class Scalar> class GnuplotInterface;

/*!
 * \ingroup InputOutput
 * \brief Interface for plotting the viscosity dependent on parameters
 */
template<class Scalar, class FluidSystem>
class PlotSolubility
{
     using FluidState = CompositionalFluidState<Scalar, FluidSystem>;

public:
    //! Constructor
    PlotSolubility()
    : numIntervals_(100)
    { }

    /*!
     * \brief Add a viscosity over temperature plot
     *
     * \param gnuplot The gnuplot interface
     */
    void addSolubilityTemperatureDependent(GnuplotInterface<Scalar> &gnuplot,
                                          int phaseIdx = 0,
                                          int compIdx = 1,
                                          Scalar pressure = 1e5,
                                          Scalar xCompOne = 0.1,
                                          Scalar lowerTemp = 273.15,
                                          Scalar upperTemp = 308.15,
                                          std::string curveName = "x_w-temp",
                                          std::string curveOptions = "w l")
    {
        std::vector<Scalar> temp(numIntervals_+1);
        std::vector<Scalar> mu(numIntervals_+1);
        Scalar tempInterval = upperTemp - lowerTemp;

        FluidState fluidState;
        fluidState.setPressure(phaseIdx, pressure);
        fluidState.setMoleFraction(phaseIdx, 0, 1-xCompOne);
        fluidState.setMoleFraction(phaseIdx, compIdx, xCompOne);

        for (int i = 0; i <= numIntervals_; i++)
        {
            temp[i] = lowerTemp + tempInterval * Scalar(i) / Scalar(numIntervals_);
            fluidState.setTemperature(temp[i]);

            if (compIdx == 1)
               mu[i] = pressure*xCompOne/BinaryCoeff::H2O_N2::henry(temp[i]);
            else if (compIdx == 2)
               mu[i] =  pressure*xCompOne/BinaryCoeff::H2O_O2::henry(temp[i]);
            else if (compIdx == 3)
               mu[i] = pressure*xCompOne/BinaryCoeff::H2O_CO2::henry(temp[i]);
            else if (compIdx == 4)
                mu[i] = pressure*xCompOne/BinaryCoeff::H2O_CH4::henry(temp[i]);
            else
                 DUNE_THROW(Dune::NotImplemented, "only four components possible");
        }

        gnuplot.setXlabel("Temperature [K]");
        gnuplot.setYlabel("x_w [-]");
        gnuplot.addDataSetToPlot(temp, mu, curveName, curveOptions);
    }
private:

    int numIntervals_;

};//end class

} // end namespace Dumux

#endif // DUMUX_PLOT_FLUID_MATRIX_LAW_HH
