// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup InputOutput
 * \brief Interface for plotting the two-phase fluid-matrix-interaction laws
 */
#ifndef DUMUX_PLOT_VISCOSITY_LAW_HH
#define DUMUX_PLOT_VISCOSITY_LAW_HH

#include <cmath>
#include <vector>
#include <string>
#include <dumux/material/fluidstates/compositional.hh>

namespace Dumux {

// forward declaration
template<class Scalar> class GnuplotInterface;

/*!
 * \ingroup InputOutput
 * \brief Interface for plotting the viscosity dependent on parameters
 */
template<class Scalar, class FluidSystem>
class PlotViscosityLaw
{
     using FluidState = CompositionalFluidState<Scalar, FluidSystem>;

public:
    //! Constructor
    PlotViscosityLaw()
    : numIntervals_(100)
    { }

    /*!
     * \brief Add a viscosity over temperature plot
     *
     * \param gnuplot The gnuplot interface
     */
    void addViscosityTemperatureDependent(GnuplotInterface<Scalar> &gnuplot,
                                          Scalar phaseIdx = 1,
                                          Scalar pressure = 1e5,
                                          Scalar xCompOne = 0,
                                          Scalar lowerTemp = 273.15,
                                          Scalar upperTemp = 308.15,
                                          std::string curveName = "mu-temp",
                                          std::string curveOptions = "w l")
    {
        std::vector<Scalar> temp(numIntervals_+1);
        std::vector<Scalar> mu(numIntervals_+1);
        Scalar tempInterval = upperTemp - lowerTemp;

        FluidState fluidState;
        fluidState.setPressure(phaseIdx, pressure);
        fluidState.setMoleFraction(phaseIdx, 1, 1-xCompOne);//this is n2
        fluidState.setMoleFraction(phaseIdx, 0, xCompOne);

        for (int i = 0; i <= numIntervals_; i++)
        {
            temp[i] = lowerTemp + tempInterval * Scalar(i) / Scalar(numIntervals_);
            fluidState.setTemperature(temp[i]);
            mu[i] = FluidSystem::viscosity(fluidState, phaseIdx);
        }

        gnuplot.setXlabel("Temperature [K]");
        gnuplot.setYlabel("dynamic viscosity [Pa*s]");
        gnuplot.addDataSetToPlot(temp, mu, curveName, curveOptions);
    }

        /*!
     * \brief Add a viscosity over mole fraction plot
     *
     * \param gnuplot The gnuplot interface
     */
    void addViscosityMoleFractionDependent(GnuplotInterface<Scalar> &gnuplot,
                                          Scalar phaseIdx = 1,
                                          Scalar pressure = 1e5,
                                          Scalar temp = 293.15,
                                          Scalar lowerxCompOne = 0.0,
                                          Scalar upperxCompOne = 1.0,
                                          std::string curveName = "mu-h2o",
                                          std::string curveOptions = "w l")
    {
        std::vector<Scalar> x(numIntervals_+1);
        std::vector<Scalar> mu(numIntervals_+1);

        Scalar xInterval = upperxCompOne - lowerxCompOne;

        FluidState fluidState;
        fluidState.setPressure(phaseIdx, pressure);
        fluidState.setTemperature(temp);

        for (int i = 0; i <= numIntervals_; i++)
        {
            x[i] = lowerxCompOne + xInterval * Scalar(i) / Scalar(numIntervals_);
            fluidState.setMoleFraction(phaseIdx, 1, 1-x[i]);//this is n2
            fluidState.setMoleFraction(phaseIdx, 0, x[i]);
            mu[i] = FluidSystem::viscosity(fluidState, phaseIdx);
        }

        gnuplot.setXlabel("Mole fraction [-]");
        gnuplot.setYlabel("dynamic viscosity [Pa*s]");
        gnuplot.addDataSetToPlot(x, mu, curveName, curveOptions);
    }

private:

    int numIntervals_;

};//end class

} // end namespace Dumux

#endif // DUMUX_PLOT_FLUID_MATRIX_LAW_HH
