#!/bin/sh

### Create a folder for the DUNE and DuMuX modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/heck2020c

### Go to specific branches
cd dune-common && git checkout releases/2.7 && cd ..
cd dune-geometry && git checkout releases/2.7 && cd ..
cd dune-grid && git checkout releases/2.7 && cd ..
cd dune-istl && git checkout releases/2.7 && cd ..
cd dune-localfunctions && git checkout releases/2.7 && cd ..
cd dune-subgrid && git checkout releases/2.6-1 && cd ..
cd dumux && git checkout a77779ea7091b331bfca1bc71fec1773fcb19ffc && cd ..
cd heck2020c && git checkout master && cd ..

# apply a patch to get k-omega calculation with density included

cd dumux && git am ../heck2020c/komega.patch && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
